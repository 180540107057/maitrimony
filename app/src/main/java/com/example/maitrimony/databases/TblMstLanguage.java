package com.example.maitrimony.databases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.maitrimony.Model.LanguageModel;
import java.util.ArrayList;

public class TblMstLanguage extends MyDataBase {
    public static final String TABLE_NAME = "TblMstLanguage";
    public static final String LANGUAGE_ID = "LanguageID";
    public static final String LANGUAGE_NAME = "LanguageName";


    public TblMstLanguage(Context context) {
        super(context);
    }
    public ArrayList<LanguageModel> getLanguages(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguageModel> list = new ArrayList<>();
        String query = "SELECT *FROM "+TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        LanguageModel languageModel= new LanguageModel();
        languageModel.setLanguageName("Select One");
        list.add(0,languageModel);

        for(int i = 0; i<cursor.getCount();i++)
        {
          LanguageModel languagemodel = new LanguageModel();
           languagemodel.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID )));
         languagemodel.setLanguageName(cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME)));
           list.add(languagemodel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;

    }
}
