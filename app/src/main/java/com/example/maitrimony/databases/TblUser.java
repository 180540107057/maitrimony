package com.example.maitrimony.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.maitrimony.Model.UserModel;
import com.example.maitrimony.util.Utils;

import java.util.ArrayList;

public class TblUser extends MyDataBase {

    public static final String TABLE_NAME = "TblUser";
    public static final String USER_ID = "UserID";
    public static final String NAME = "Name";
    public static final String FATHER_NAME = "FatherName";
    public static final String SUR_NAME = "SurName";
    public static final String GENDER = "Gender";
    public static final String HOBBIES = "Hobbies";
    public static final String DOB = "Dob";
    public static final String PHONE_NO = "PhoneNo";
    public static final String CITY_ID = "CityID";
    public static final String LANGUAGE_ID = "LanguageID";
    public static final String EMAIL = "Email";

    /* Query column for cityName and language*/
    public static final String LANGUAGE_NAME = "LanguageName";
    public static final String CITY_NAME = "CityName";
    public static final String IS_FAVORITE = "Isfavorite";


    public TblUser(Context context) {
        super(context);
    }

    public UserModel getCreatedModelUsingCursor(Cursor cursor) {
        UserModel usermodel;
        usermodel = new UserModel();
        usermodel.setUserID(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        usermodel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        usermodel.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        usermodel.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
        usermodel.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        usermodel.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
        usermodel.setDob(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(DOB))));
        usermodel.setPhoneNO(cursor.getString(cursor.getColumnIndex(PHONE_NO)));
        usermodel.setCityID(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        usermodel.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
        usermodel.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        usermodel.setLanguageName(cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME)));
        usermodel.setCityName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
        usermodel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
        return usermodel;

    }

    public ArrayList<UserModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT " +
                " UserID," +
                " Name," +
                " FatherName," +
                " SurName," +
                " Gender," +
                " Dob," +
                " PhoneNo," +
                "Email," +
                "Hobbies," +
                " Isfavorite," +
                " TblMstLanguage.LanguageID," +
                " TblMstCity.CityID," +
                " LanguageName," +
                " CityName " +
                " FROM " +
                " TblUser" +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                " INNER JOIN TblMstCity on TblUser.CityID = TblMstCity.CityID";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }


    public ArrayList<UserModel> getUserListByGender(int gender) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT " +
                " UserID," +
                " Name," +
                " FatherName," +
                " SurName," +
                " Gender," +
                " Dob," +
                " PhoneNo," +
                "Email," +
                "Hobbies," +
                " Isfavorite," +
                " TblMstLanguage.LanguageID," +
                " TblMstCity.CityID," +
                " LanguageName," +
                " CityName " +
                " FROM " +
                " TblUser" +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                " INNER JOIN TblMstCity on TblUser.CityID = TblMstCity.CityID" +
                " WHERE " +
                " Gender = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public ArrayList<UserModel> getFavoriteUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT " +
                " UserID," +
                " Name," +
                " FatherName," +
                " SurName," +
                " Gender," +
                " Dob," +
                " PhoneNo," +
                "Email," +
                "Hobbies," +
                " Isfavorite," +
                " TblMstLanguage.LanguageID," +
                " TblMstCity.CityID," +
                " LanguageName," +
                " CityName " +
                " FROM " +
                " TblUser" +
                " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                " INNER JOIN TblMstCity on TblUser.CityID = TblMstCity.CityID" +
                " WHERE " +
                " Isfavorite = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(1)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }


    public long insertUser(String name, String fatherName, String SurName, int gender, String dob, String hobbies, String phoneNo, int languageID, int cityID, String Email, int is_favorite) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SUR_NAME, SurName);
        cv.put(GENDER, gender);
        cv.put(DOB, dob);
        cv.put(HOBBIES, hobbies);
        cv.put(EMAIL, Email);
        cv.put(PHONE_NO, phoneNo);
        cv.put(LANGUAGE_ID, languageID);
        cv.put(CITY_ID, cityID);
        cv.put(IS_FAVORITE, is_favorite);
        long lastInsertedId = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertedId;
    }

    public int updateUserById(int userId, String name, String fatherName, String SurName, int gender, String dob, String hobbies, String phoneNo, int languageID, int cityID, String Email, int is_favorite) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SUR_NAME, SurName);
        cv.put(GENDER, gender);
        cv.put(DOB, dob);
        cv.put(HOBBIES, hobbies);
        cv.put(PHONE_NO, phoneNo);
        cv.put(LANGUAGE_ID, languageID);
        cv.put(CITY_ID, cityID);
        cv.put(EMAIL, Email);
        cv.put(IS_FAVORITE, is_favorite);
        int lastUpdatedId = db.update(TABLE_NAME, cv, USER_ID + " = ? ", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdatedId;
    }

    public int deleteUserById(int userId) {
        SQLiteDatabase db = getWritableDatabase();
        int deletedUserID = db.delete(TABLE_NAME, USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return deletedUserID;
    }

    public int forFavoriteStatus(int isFavorite, int userId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(IS_FAVORITE, isFavorite);
        cv.put(USER_ID, userId);
        int lastUpdatedId = db.update(TABLE_NAME, cv, USER_ID + " = ? ", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdatedId;

    }
}

