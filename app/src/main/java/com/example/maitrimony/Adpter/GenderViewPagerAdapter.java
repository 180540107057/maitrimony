package com.example.maitrimony.Adpter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.example.maitrimony.fragment.UserListFragment;
import com.example.maitrimony.util.Constant;

public class GenderViewPagerAdapter extends FragmentStatePagerAdapter {

    private String tabTitle[] = new String[]{"Male", "Female"};
    private Context context;

    public GenderViewPagerAdapter(@NonNull FragmentManager fm, int behavior, Context context) {
        super(fm, behavior);
        this.context = context;
    }


    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return UserListFragment.getInstance(Constant.MALE);
        } else {
            return UserListFragment.getInstance(Constant.FEMALE);
        }
    }

    @NonNull
    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitle[position];
    }
}
