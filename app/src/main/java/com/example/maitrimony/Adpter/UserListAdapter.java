package com.example.maitrimony.Adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.maitrimony.Model.UserModel;
import com.example.maitrimony.R;
import com.example.maitrimony.util.Utils;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;


public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {
    Context context;
    ArrayList<UserModel> userList;
    OnViewClickListener onViewClickListener;

    public UserListAdapter(Context context, ArrayList<UserModel> userList, OnViewClickListener onViewClickListener) {
        this.context = context;
        this.userList = userList;
        this.onViewClickListener = onViewClickListener;

    }


    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        holder.tvFullName.setText(userList.get(position).getName() + " " + userList.get(position).getSurName());
        holder.tvLanguageCity.setText(userList.get(position).getLanguageName() + " | " + userList.get(position).getCityName());
        String dateParts[] = userList.get(position).getDob().split("/");
        String age = Utils.getAge(Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[1]), Integer.parseInt(dateParts[0]));
        holder.tvAge.setText("Age : " + age);

        holder.ivFavoriteUser.setImageResource(userList.get(position).getIsFavorite() == 1 ? R.drawable.baseline_star_black_36 :
                R.drawable.baseline_star_border_black_36);

        holder.ivDeleteUser.setOnClickListener(v -> {
            if (onViewClickListener != null) {
                onViewClickListener.OnClick(position);
            }

        });

        holder.itemView.setOnClickListener(v -> {
            if (onViewClickListener != null) {
                onViewClickListener.OnItemClick(position);
            }

        });

        holder.ivFavoriteUser.setOnClickListener(v -> {
            if (onViewClickListener != null) {
                onViewClickListener.onFavoriteClick(position);
            }

        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public interface OnViewClickListener {
        void OnClick(int position);

        void OnItemClick(int position);

        void onFavoriteClick(int position);
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullName)
        TextView tvFullName;
        @BindView(R.id.tvLanguageCity)
        TextView tvLanguageCity;
        @BindView(R.id.tvAge)
        TextView tvAge;
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavoriteUser)
        ImageView ivFavoriteUser;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}

