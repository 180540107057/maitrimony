package com.example.maitrimony.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.example.maitrimony.Adpter.GenderViewPagerAdapter;
import com.example.maitrimony.R;
import com.example.maitrimony.util.Constant;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class ActivityUserListByGender extends BaseActivity {
    @BindView(R.id.tlGenders)
    TabLayout tlGenders;
    @BindView(R.id.vpUserList)
    ViewPager vpUserList;
    GenderViewPagerAdapter adapter;
    MyFavoriteReceiver receiver = new MyFavoriteReceiver();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender_wise_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_user_list), true);
        setUpViewPagerAdapter();
        registerReceiver(receiver, new IntentFilter(Constant.FAVORITE_CHANGE_FILTER));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favorite_user_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.ivFavoriteUser) {
            Intent intent = new Intent(this, UserListActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        if (vpUserList != null) {
            setUpViewPagerAdapter();
        }
        super.onResume();
    }

    void setUpViewPagerAdapter() {
        adapter = new GenderViewPagerAdapter(getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, this);
        vpUserList.setAdapter(adapter);
        tlGenders.setupWithViewPager(vpUserList);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ActivityUserListByGender.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    class MyFavoriteReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

        }
    }
}
