package com.example.maitrimony.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.maitrimony.Adpter.UserListAdapter;
import com.example.maitrimony.Model.UserModel;
import com.example.maitrimony.R;
import com.example.maitrimony.databases.TblUser;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends BaseActivity {
    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_user_list), true);
        setAdapter();
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure To Delete This User?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (DialogInterface dialog, int which) -> {
                    int deletedUserId = new TblUser(this).deleteUserById(userList.get(position).getUserID());
                    if (deletedUserId > 0) {
                        Toast.makeText(this, "Deleted SuccessFully!", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(this, "Something Went Wrong!", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (DialogInterface dialog, int which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getFavoriteUserList());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnClick(int position) {
                showAlertDialog(position);

            }

            @Override
            public void OnItemClick(int position) {

            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUserID = new TblUser(UserListActivity.this).forFavoriteStatus(0, userList.get(position).getUserID());
                if (lastUpdatedUserID > 0) {
                    userList.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(0, userList.size());
                }

            }
        });
        rcvUserList.setAdapter(adapter);
    }
}
