package com.example.maitrimony.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.example.maitrimony.Adpter.CityAdapter;
import com.example.maitrimony.Adpter.LanguageAdapter;
import com.example.maitrimony.Model.CityModel;
import com.example.maitrimony.Model.LanguageModel;
import com.example.maitrimony.Model.UserModel;
import com.example.maitrimony.R;
import com.example.maitrimony.databases.TblMstCity;
import com.example.maitrimony.databases.TblMstLanguage;
import com.example.maitrimony.databases.TblUser;
import com.example.maitrimony.util.Constant;
import com.example.maitrimony.util.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddUserActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etFatherName)
    EditText etFatherName;
    @BindView(R.id.etSurName)
    EditText etSurName;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.rbFeMale)
    RadioButton rbFeMale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etDob)
    EditText etDob;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.etEmailAddress)
    EditText etEmailAddress;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;


    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> cityList = new ArrayList<>();
    ArrayList<LanguageModel> languageList = new ArrayList<>();
    String startingDate = "1990-07-19T12:00:00";

    UserModel userModel;
    @BindView(R.id.cbActSport)
    CheckBox cbActSport;
    @BindView(R.id.cbActMusic)
    CheckBox cbActMusic;
    @BindView(R.id.cbActCooking)
    CheckBox cbActCooking;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_add_user), true);
        setDateToView();
        getDataForUpdate();

    }

    public String getHobbiesInString() {
        String tempHobbies = " ";
        if (cbActSport.isChecked() && cbActMusic.isChecked() && cbActCooking.isChecked()) {
            tempHobbies = cbActSport.getText().toString() + "," + cbActMusic.getText().toString() + "," +
                    cbActCooking.getText().toString();
        } else if (cbActSport.isChecked() && cbActMusic.isChecked()) {
            tempHobbies = cbActSport.getText().toString() + "," + cbActMusic.getText().toString();
        } else if (cbActCooking.isChecked() && cbActMusic.isChecked()) {
            tempHobbies = cbActMusic.getText().toString() + "," + cbActCooking.getText().toString();
        } else if (cbActSport.isChecked() && cbActCooking.isChecked()) {
            tempHobbies = cbActSport.getText().toString() + "," + cbActCooking.getText().toString();
        } else if (cbActSport.isChecked()) {
            tempHobbies = cbActSport.getText().toString();
        } else if (cbActMusic.isChecked()) {
            tempHobbies = cbActMusic.getText().toString();
        } else {
            tempHobbies = cbActCooking.getText().toString();
        }

        return tempHobbies;
    }

    void getDataForUpdate() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle(R.string.lbl_edit_user);
            etName.setText(userModel.getName());
            etFatherName.setText(userModel.getFatherName());
            etSurName.setText(userModel.getSurName());
            etPhoneNumber.setText(userModel.getPhoneNO());
            etDob.setText(userModel.getDob());
            if (userModel.getGender() == Constant.MALE) {
                rbMale.setChecked(true);
            } else {
                rbFeMale.setChecked(true);
            }
            etEmailAddress.setText(userModel.getEmail());
            spCity.setSelection(getSelectedPositionFromCityId(userModel.getCityID()));
            spLanguage.setSelection(getSelectedPositionFromLanguageId(userModel.getLanguageID()));

        }
    }

    int getSelectedPositionFromCityId(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCityID() == cityId) {
                return i;
            }
        }
        return 0;
    }

    int getSelectedPositionFromLanguageId(int languageId) {
        for (int i = 0; i < languageList.size(); i++) {
            if (cityList.get(i).getCityID() == languageId) {
                return i;
            }
        }
        return 0;
    }

    void setSpinnerAdapter() {
        cityList.addAll(new TblMstCity(this).getCityList());
        languageList.addAll(new TblMstLanguage(this).getLanguages());

        cityAdapter = new CityAdapter(this, cityList);
        languageAdapter = new LanguageAdapter(this, languageList);

        spCity.setAdapter(cityAdapter);
        spLanguage.setAdapter(languageAdapter);

    }

    void setDateToView() {
        final Calendar newCalendar = Calendar.getInstance();
        etDob.setText(String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" + String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" + newCalendar.get(Calendar.YEAR));
        setSpinnerAdapter();
    }

    @OnClick(R.id.etDob)
    public void onEtDobClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(startingDate);
        newCalendar.setTimeInMillis(date.getTime());
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddUserActivity.this, (datePicker, year, month, day_of_month) -> {
            etDob.setText(String.format("%02d", day_of_month) + "/" + String.format("%02d", (month + 1)) + "/" + year);

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {
        if (isValidUser()) {
            if (userModel == null) {
                long lastInsertedId = new TblUser(getApplicationContext()).insertUser(etName.getText().toString(), etFatherName.getText().toString(), etSurName.getText().toString()
                        , rbMale.isChecked() ? Constant.MALE : Constant.FEMALE, Utils.getFormatedDateToInsert(etDob.getText().toString()), getHobbiesInString(),
                        etPhoneNumber.getText().toString(), languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(), cityList.get(spCity.getSelectedItemPosition()).getCityID(), etEmailAddress.getText().toString(), 0);
                showToast(lastInsertedId > 0 ? "User Inserted Successfully" : "Something Went Wrong! ");
            } else {
                long lastInsertedId = new TblUser(getApplicationContext()).updateUserById(userModel.getUserID(), etName.getText().toString(), etFatherName.getText().toString(), etSurName.getText().toString()
                        , rbMale.isChecked() ? Constant.MALE : Constant.FEMALE, Utils.getFormatedDateToInsert(etDob.getText().toString()), getHobbiesInString(),
                        etPhoneNumber.getText().toString(), languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(), cityList.get(spCity.getSelectedItemPosition()).getCityID(), etEmailAddress.getText().toString(), userModel.getIsFavorite());
                showToast(lastInsertedId > 0 ? "User Updated Successfully" : "Something Went Wrong!");

            }
            Intent intent = new Intent(AddUserActivity.this, ActivityUserListByGender.class);
            startActivity(intent);
            finish();
        }

    }


    boolean isValidUser() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            isValid = false;
            etName.setError(getString(R.string.lbl_name));
        }
        if (TextUtils.isEmpty(etFatherName.getText().toString())) {
            isValid = false;
            etFatherName.setError(getString(R.string.error_father_name));
        }
        if (TextUtils.isEmpty(etSurName.getText().toString())) {
            isValid = false;
            etSurName.setError(getString(R.string.error_enter_surname));
        }
        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_enter_phone));
        } else if (etPhoneNumber.getText().toString().length() < 10) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_valid_phone));
        }

        if (TextUtils.isEmpty(etEmailAddress.getText().toString())) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_enter_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.getText().toString()).matches()) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_valid_email));
        }
        if (spCity.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_city_select));
        }
        if (spLanguage.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_language_select));
        }
        if (!(cbActCooking.isChecked() || cbActMusic.isChecked() || cbActSport.isChecked())) {
            isValid = false;
            showToast(getString(R.string.error_check_hobby));
        }
        return isValid;
    }

    @OnClick(R.id.cbActSport)
    public void onCbActSportClicked() {
    }

    @OnClick(R.id.cbActMusic)
    public void onCbActMusicClicked() {
    }

    @OnClick(R.id.cbActCooking)
    public void onCbActCookingClicked() {
    }
}
