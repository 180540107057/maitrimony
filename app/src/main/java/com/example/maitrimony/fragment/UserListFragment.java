package com.example.maitrimony.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.maitrimony.Adpter.UserListAdapter;
import com.example.maitrimony.Model.UserModel;
import com.example.maitrimony.R;
import com.example.maitrimony.activity.AddUserActivity;
import com.example.maitrimony.databases.TblUser;
import com.example.maitrimony.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserListFragment extends Fragment {
    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;


    public static UserListFragment getInstance(int gender) {
        UserListFragment fragment = new UserListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.GENDER, gender);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_list, null);
        ButterKnife.bind(this, v);
        setHasOptionsMenu(true);
        setAdapter();
        return v;
    }


    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure To Delete This User?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (DialogInterface dialog, int which) -> {
                    int deletedUserId = new TblUser(getActivity()).deleteUserById(userList.get(position).getUserID());
                    if (deletedUserId > 0) {
                        Toast.makeText(getActivity(), "Deleted SuccessFully!", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(getActivity(), "Something Went Wrong!", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (DialogInterface dialog, int which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        userList.addAll(new TblUser(getActivity()).getUserListByGender(getArguments().getInt(Constant.GENDER)));
        adapter = new UserListAdapter(getActivity(), userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void OnItemClick(int position) {
                Intent intent = new Intent(getActivity(), AddUserActivity.class);
                intent.putExtra(Constant.USER_OBJECT, userList.get(position));
                startActivity(intent);

            }

            @Override
            public void onFavoriteClick(int position) {

                int lastUpdatedUserID = new TblUser(getActivity()).forFavoriteStatus(userList.get(position).getIsFavorite() == 0 ? 1 : 0, userList.get(position).getUserID());
                if (lastUpdatedUserID > 0) {
                    userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    adapter.notifyItemChanged(position);
                }
            }


        });

        rcvUserList.setAdapter(adapter);
        checkAndVisibleView();
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

    void openAddUserScreen() {
        Intent intent = new Intent(getActivity(), AddUserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnAddUser)
    public void onViewClicked() {
        openAddUserScreen();
    }
}
