package com.example.maitrimony.Model;

import java.io.Serializable;
import java.util.Calendar;

public class UserModel implements Serializable {
    int UserID;
    String Name;
    String FatherName;
    String SurName;
    int Gender;
    String Hobbies;
    String Email;
    String Dob;
    String PhoneNO;
    String LanguageName;
    String CityName;
    int CityID;
    int LanguageID;
    int IsFavorite;

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getPhoneNO() {
        return PhoneNO;
    }

    public void setPhoneNO(String phoneNO) {
        PhoneNO = phoneNO;
    }

    public int getCityID() {
        return CityID;
    }

    public void setCityID(int cityID) {
        CityID = cityID;
    }

    public int getLanguageID() {
        return LanguageID;
    }

    public void setLanguageID(int languageID) {
        LanguageID = languageID;
    }

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "UserID=" + UserID +
                ", Name='" + Name + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", SurName='" + SurName + '\'' +
                ", Gender=" + Gender +
                ", Hobbies='" + Hobbies + '\'' +
                ", Email='" + Email + '\'' +
                ", Dob='" + Dob + '\'' +
                ", PhoneNO='" + PhoneNO + '\'' +
                ", LanguageName='" + LanguageName + '\'' +
                ", CityName='" + CityName + '\'' +
                ", CityID=" + CityID +
                ", LanguageID=" + LanguageID +
                ", IsFavorite=" + IsFavorite +
                '}';
    }
}
