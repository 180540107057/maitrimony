package com.example.maitrimony.Model;

import java.io.Serializable;

public class CityModel implements Serializable {
    int CityID;
    String CityName;


    public int getCityID() {
        return CityID;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public void setCityID(int cityID) {
        CityID = cityID;
    }
    @Override
    public String toString() {
        return "CityModel{" +
                "CityID=" + CityID +
                ", Name='" + CityName+ '\'' +
                '}';
    }

}
