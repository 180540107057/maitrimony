package com.example.maitrimony.Model;

import java.io.Serializable;

public class LanguageModel implements Serializable {
    int LanguageID;
    String LanguageName;

    public int getLanguageID() {
        return LanguageID;
    }

    public void setLanguageID(int languageID) {
        LanguageID = languageID;
    }

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }

    @Override
    public String toString() {
        return "Languagemodel{" +
                "LanguageID=" + LanguageID +
                ", LanguageName='" + LanguageName + '\'' +
                '}';
    }
}
